{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      <home-manager/nixos>
      ./override/fcgiwrap.nix
      ./override/phpfpm.nix
    ];
  disabledModules = [ "services/web-servers/fcgiwrap.nix" "services/web-servers/phpfpm/default.nix" ]; # Overrides
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  #boot.kernelPackages = pkgs.linuxPackages_latest-libre;
  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
boot.supportedFilesystems = [ "zfs" ];
networking.hostId = "38661127";
  networking.hostName = "verntil"; # Define your hostname.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.
  networking.nameservers = [ "45.90.28.127" ];
services.zfs.autoScrub.enable = true;
services.iperf3.enable = true;
  # Set your time zone.
  time.timeZone = "UTC";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  security.sudo.wheelNeedsPassword = false;
  environment.systemPackages = with pkgs; [
    zfs
    coreutils-full
    check-uptime
    vim 
    wget
    curl
    home-manager
    audit
    cached-nix-shell
    iptables
    zsh
    fish
    dash
    nushell
    ksh
    tcsh
    neofetch
    emacs
    neovim
    git
    profanity
    tut
    irssi
    weechat
    neomutt
    gomuks
    python310
    python310Packages.pip
    figlet
    lua
    nix-index
    aerc
  ];

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    permitRootLogin = "prohibit-password";
    extraConfig = "TCPKeepAlive yes\nClientAliveInterval 30\nClientAliveCountMax 99999\nX11UseLocalhost no\nX11DisplayOffset 10\nMatch address 192.168.122.1\n\tPasswordAuthentication yes";
    forwardX11 = true;
  };
  services.acpid.enable = true;

  environment.extraInit = "if echo \"$-\" | grep i > /dev/null; then bash /etc/motd.script; if [ -r ~/README ]; then printf '\nHello %s, there is a README file in your home directory. See the contents below:\n\n%s\n' $USER \"$(<~/README)\"; fi; if [ -r /var/tmp/poll ]; then printf '\nTHERE IS AN ONGOING POLL.\n%s\nPoll ends on %s\nPlace your vote in ~/.vote\n' \"$(</var/tmp/poll)\" \"$(date -d @$(($(stat -c %Y /var/tmp/poll)+1159300)))\"; fi; fi";
  security.auditd.enable = true;
  services.postgresql = {
    enable = true;
    enableTCPIP = true;
  };
#  services.phpfpm.pools.mypool.socket = "";
  services.phpfpm.pools.mypool = {
    user = "php";
    group = "php";
    phpPackage = pkgs.php;
    listen = "192.168.122.30:9000";
    settings = {
      "pm" = "dynamic";
      "pm.max_children" = 75;
      "pm.start_servers" = 10;
      "pm.min_spare_servers" = 5;
      "pm.max_spare_servers" = 20;
      "pm.max_requests" = 500;
#      "pm.status_listen" = "127.0.0.1:9000";
      "catch_workers_output" = true;
      
    };
  };
  services.fcgiwrap.user = "php";
  services.fcgiwrap.group = "php";
  services.fcgiwrap.enable = true;
  services.fcgiwrap.socketType = "tcp";
  services.fcgiwrap.socketAddress = "192.168.122.30:9001";
  networking.firewall.enable = false;
  networking.nftables.enable = true;
services.syslogd.enable = true;
services.journald.extraConfig = "ForwardToWall=no";
virtualisation.docker.enable = true;
virtualisation.lxc.enable = true;
    systemd.services.gemini = {
      description = "Gemini Server";
      after = [ "network-online.target" ];
      wants = [ "network-online.target" ]; # systemd-networkd-wait-online.service
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
	ExecStartPre = "/var/gemini/conf/updateusersites";
        ExecStart = "/var/gemini/gemserv/target/release/gemserv /var/gemini/conf/gemserv.toml";
        Type = "simple";
        User = "gemini";
        Group = "gemini";
        Restart = "on-abnormal";
        KillMode = "mixed";
        KillSignal = "SIGQUIT";
        TimeoutStopSec = "5s";
      };
};
systemd.services.rathole = {
description = "Forward ports";
after = [ "network-online.target" ];
wants = [ "network-online.target" ];
wantedBy = ["multi-user.target"];
serviceConfig = {
ExecStart = "/root/rathole -c /etc/rathole.toml";
Type = "simple";
Restart = "on-abnormal";
KillMode = "mixed";
KillSignal = "SIGQUIT";
TimeotStopSec = "5s";
};
};
services.xinetd.enable = true;
#services.xinetd.services = [ vger
services.xinetd.services = [ {
  name = "gophernicus";
  user = "gopher";
  server = "/var/lib/gopher/gophernicus";
  serverArgs = "-r/var/lib/gopher -hvern.cc";
  protocol = "tcp";
  port = 70;
  unlisted = true;
} ];
programs.command-not-found.enable = false;
programs.bash.interactiveShellInit = ''
  source ${pkgs.nix-index}/etc/profile.d/command-not-found.sh
'';
  services.cron = {
    enable = true;
    systemCronJobs = [
      "@weekly      root         cd /var/lib/nix-index && nix-shell -p nix-index --run 'nix-index --db ./x86_64-linux-index' --option system x86_64-linux --extra-platforms x86_64-linux && cp files files.bak && cp x86_64-linux-index/files ./files"
    ];
  };
  networking.wireguard.enable = true;
  networking.wg-quick.interfaces = {
    wg0 = {
      address = [ "10.7.0.4/24" ];
      dns = [ "192.168.122.1" ];
      privateKeyFile = "/root/wg-privkey";
      peers = [
        {
          # Public key of the server (not a file path).
          publicKey = "ylsR8JgSF9RuIFW+3EKHbLAA1PZYCFG/OnCuJtZlXl4=";
	  presharedKeyFile = "/root/wg-psk";
          # Forward all the traffic via VPN.
          allowedIPs = [ "0.0.0.0/0" ];
          # Set this to the server IP and port.
          endpoint = "5.161.108.85:51820";
          # Send keepalives every 25 seconds. Important to keep NAT tables alive.
          persistentKeepalive = 25;
        }
      ];
    };
  };
security.pam.services.su.text = "account required pam_unix.so\nauth sufficient pam_rootok.so\nauth required pam_faillock.so\nauth required pam_deny.so\npassword sufficient pam_unix.so nullok sha512\nsession required pam_env.so conffile=/etc/pam/environment readenv=0\nsession required pam_unix.so";
system.stateVersion = "22.05"; # Did you read the comment?

}

