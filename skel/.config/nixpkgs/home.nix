{ config, pkgs, ... }:
## Please read the home-configuration.nix manpage for a list of all available options.
{
  home.username = "USER";
  home.homeDirectory = "/home/USER";
  home.stateVersion = "22.05";
  home.packages = with pkgs; [
    nix-index
  ];
  programs.fzf.enable = true;
  programs.fzf.enableBashIntegration = true;
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
