# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
	for file in ~/.config/bash/*; do
		. "$file"
	done
fi

export PATH="/usr/local/bin${PATH:+:}$PATH"
mesg n 2> /dev/null || true
